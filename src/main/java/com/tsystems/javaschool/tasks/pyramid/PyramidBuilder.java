package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        Double dSide = ((java.lang.Math.sqrt(inputNumbers.size() * 8 + 1)) - 1) / 2;
        if (dSide.isNaN() || dSide.intValue() < dSide) {
            throw new CannotBuildPyramidException();
        }
        int side = dSide.intValue();
        int width = side*2-1;
        int center = width/2;
        int array[][] = new int[side][width];
        int listPos = inputNumbers.size()- 1;

        try {
            Collections.sort(inputNumbers, Collections.reverseOrder());
        }
        catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
        for (int i = 0; i < side; i++) {
            int pos = center - i;
            for (int j = 0; j <= i; j++) {
                array[i][pos] = inputNumbers.get(listPos);
                listPos--;
                pos+=2;
            }
        }
        return array;
    }


}

package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (!(x instanceof List && y instanceof List)) {
            throw new IllegalArgumentException();
        }
        int prev_ord = -1;
        int next_ord;
        for (Object element:x) {
            next_ord = y.indexOf(element);
            if (next_ord <= prev_ord) {
                return false;
            }
            prev_ord = next_ord;
        }

        return true;
    }
}

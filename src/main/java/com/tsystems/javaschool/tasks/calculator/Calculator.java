package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private enum Operator {
        ADD(1), SUBTRACT(2), MULTIPLY(3), DIVIDE(4);
        final int precedence;
        Operator(int p) { precedence = p; }
    }

    private static Map<String, Operator> ops = new HashMap<String, Operator>() {{
        put("+", Operator.ADD);
        put("-", Operator.SUBTRACT);
        put("*", Operator.MULTIPLY);
        put("/", Operator.DIVIDE);
    }};

    private List infix(String statement) {
        List parsed = new ArrayList();
        String number = new String();
        for (char c: statement.toCharArray()) {
            if (Character.isDigit(c) || c=='.') {
                number += c;
            }
            else if (number != "") {
                parsed.add(number);
                number = "";
            }
            if ("+-*/".indexOf(c) >= 0) {
                parsed.add(ops.get(""+c));
            }
            else if ("()".indexOf(c) >= 0) {
                parsed.add(c);
            }
        }
        if (!number.equals("")) parsed.add(number);


        return parsed;
    }

    private List postfix(List infix) {
        List rpn = new LinkedList();
        Deque stack = new LinkedList();
        for (Object token: infix) {
            if (token instanceof String) {
                rpn.add(token);
            }
            else if (token instanceof Operator) {
                if (token instanceof Operator) {
                    while (!stack.isEmpty() && stack.peek() instanceof Operator && ((Operator) token).precedence
                            <= ((Operator) stack.peek()).precedence) {
                        rpn.add(stack.pop());
                    }
                    stack.push(token);
                }
            }
            else if (token.equals(')')) {
                while (!stack.isEmpty() && !stack.peek().equals('(')) {
                    rpn.add(stack.pop());
                }
                if (stack.isEmpty()) {
                    return null;
                }
                stack.pop();
            }
            else if (token.equals('(')) {
                stack.push(token);
            }
        }
        while (!stack.isEmpty()) {
            rpn.add(stack.pop());
        }
        return rpn;
    }

    private OptionalDouble calc(List postfix) {
        Deque<Double> stack = new LinkedList();
        double op1;
        double op2;
        double result;
        for (Object token:postfix) {
            if (token instanceof String && token != "") {
                try {
                    stack.push(Double.parseDouble((String) token));
                }
                catch (Exception e) {
                    return OptionalDouble.empty();
                }
            }
            else if (token instanceof Operator) {
                if (stack.size() > 1) {
                    op2 = stack.pop();
                    op1 = stack.pop();
                }
                else {
                    return OptionalDouble.empty();
                }
                if (token == Operator.ADD) {
                    stack.push(op1 + op2);
                }
                else if (token == Operator.SUBTRACT) {
                    stack.push(op1 - op2);
                }
                else if (token == Operator.MULTIPLY) {
                    stack.push(op1 * op2);
                }
                else if (token == Operator.DIVIDE) {
                    if (op2 == 0) {
                        return OptionalDouble.empty();
                    }
                    stack.push(op1 / op2);
                }
            }
        }
        if (!stack.isEmpty()){
            result = stack.pop();
        }
        else {
            return OptionalDouble.empty();
        }
        if (stack.isEmpty()) {
            return OptionalDouble.of(result);
        }
        else {
            return OptionalDouble.empty();
        }
    }

    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        List infix = infix(statement);
        List postfix = postfix(infix);
        if (postfix == null) {
            return null;
        }
        OptionalDouble result = calc(postfix);
        if (result.isPresent()) {
            Double d = result.getAsDouble();
            int i = d.intValue();
            if (i < d) {
                return String.valueOf(d);
            }
            else {
                return String.valueOf(i);
            }
        }
        else {
            return null;
        }
    }

}
